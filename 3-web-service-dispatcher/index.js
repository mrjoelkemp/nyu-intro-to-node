const http = require('http');
const dispatcher = require('httpdispatcher');

// Only used for debug printing
const url = require('url');
const querystring = require('querystring');

const server = http.createServer((request, response) => {
  console.log('request method:', request.method);
  console.log('request url:', request.url);
  console.log('parsed params:', querystring.parse(url.parse(request.url).query));

  try {
    console.log(request.url);

    dispatcher.dispatch(request, response);
  } catch(err) {
    console.log(err);
  }
});

const port = 8000;

server.listen(port, () => {
  console.log('Server listening on: http://localhost:' + port);
});

dispatcher.onGet("/", function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Homepage');
});

// If we wanted to create a message
dispatcher.onPost("/message", function(req, res) {
    res.writeHead(201, {'Content-Type': 'text/plain'});
    res.end('Message created!');
});
