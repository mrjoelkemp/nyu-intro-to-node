const http = require('http');

// Only used for debug printing
const url = require('url');
const querystring = require('querystring');

const server = http.createServer((request, response) => {
  console.log('request method:', request.method);
  console.log('request url:', request.url);
  console.log('parsed params:', querystring.parse(url.parse(request.url).query));

  response.end('Path hit: ' + request.url);
});

const port = 8000;

server.listen(port, () => {
  console.log('Server listening on: http://localhost:' + port);
});
