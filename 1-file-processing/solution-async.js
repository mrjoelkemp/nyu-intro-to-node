const fs = require('fs');

const content = fs.readFile('sample.txt', 'utf8', (err, content) => {
  if (err) {
    console.log('Unable to read file. ', err.message);
    return;
  }

  const strippedContent = stripPunctuation(content);
  const words = strippedContent.split(' ');

  const wordCounts = {};

  words.forEach(word => {
    word = word.trim().toLowerCase();

    if (wordCounts[word] === undefined) {
      wordCounts[word] = 0;
    }

    wordCounts[word]++;
  });

  for (word in wordCounts) {
    console.log(word + ':', wordCounts[word]);
  }
});

function stripPunctuation(str) {
  return str.replace(/[&\/\\#,+\(\)$~%\.!^'"\;:*?\[\]<>{}]/g, '');
}
